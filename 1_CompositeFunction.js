// Tomairo Huamanraime Juan Junior
// Prueba RECYLINK - Algorithm and Data Structure Questions

/* 1 - Composite function */
function rokket(a) {
    return (b) => {
        return (c) => {
            return a * b * c;
        }
    }
}
console.log(rokket(2)(5)(3));
console.log(rokket(4)(2)(2));
console.log(rokket(8)(2)(1));