// Tomairo Huamanraime Juan Junior
// Prueba RECYLINK - Algorithm and Data Structure Questions

/* 2 - Longest string */
function rokket(listString){
    var maxString = '';
    for (var i = 0; i < listString.length; i++){
        maxString = listString[i].length > maxString.length ? listString[i]:maxString;
    }
    return maxString;
} 

const list = ['best', 'company', 'ever'];
console.log(rokket(list));