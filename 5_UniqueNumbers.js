// Tomairo Huamanraime Juan Junior
// Prueba RECYLINK - Algorithm and Data Structure Questions

/* 5 - Unique numbers */
function rokket(arrNum1, arrNum2){

    var arrUnion = [];

    for (var i = 0; i < arrNum1.length; i++){
        if (!arrUnion.includes(arrNum1[i])){
            arrUnion.push(arrNum1[i]);
        }
    }

    for (var i = 0; i < arrNum2.length; i++){
        if (!arrUnion.includes(arrNum2[i])){
            arrUnion.push(arrNum2[i]);
        }
    }

    return arrUnion.sort();

}

console.log(rokket([1, 2, 5], [2, 1, 6]));
console.log(rokket([1, 2, 3], [4, 5, 6]));