// Tomairo Huamanraime Juan Junior
// Prueba RECYLINK - Algorithm and Data Structure Questions


/* 4 - Only last names */
function rokket(listContact){

    listApellido = [];
    for (var i = 0; i < listContact.length; i++){
        listApellido.push(listContact[i].lastName);
    }
    return listApellido;

}

const contacts = [
    { firstName: 'Juanito', lastName: 'Rokket' },
    { firstName: 'James', lastName: 'Bond' },
    { firstName: 'Harry', lastName: 'Potter' }
    ];

console.log(rokket(contacts));